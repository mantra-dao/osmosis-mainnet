FROM ubuntu:20.04
LABEL maintainer="info@axonibyte.com"
RUN apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install curl jq bash -y \
    && addgroup --gid 2000 chainsvc \
    && useradd --no-create-home chainsvc -s /bin/false -u 2000 -g chainsvc
COPY build/libwasmvm.x86_64.so /usr/lib/libwasmvm.x86_64.so
COPY build/osmosisd /bin/osmosisd
RUN chmod +x /bin/osmosisd
STOPSIGNAL SIGTERM
USER chainsvc
WORKDIR /chain
ENTRYPOINT ["/bin/osmosisd", "start", "--home", "/chain"]
